package com.zhji.babbelinterview.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhji.babbelinterview.BabbelApp;
import com.zhji.babbelinterview.models.Word;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

/**
 * Created by zhenlei on 16/04/16.
 */
public class LoadFileUtils {
    public static Observable<List<Word>> readFile(String file) {
        return Observable.create(new Observable.OnSubscribe<List<Word>>() {
            @Override
            public void call(Subscriber<? super List<Word>> subscriber) {
                BufferedReader reader = null;
                StringBuilder text = new StringBuilder();
                try {
                    reader = new BufferedReader(new InputStreamReader(BabbelApp.getContext().getAssets().open(file)));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        text.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                    return;
                } finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                            subscriber.onError(e);
                            return;
                        }
                    }
                }
                List<Word> words = new Gson().fromJson(text.toString().replaceAll("\\s+", " "), new TypeToken<List<Word>>() {
                }.getType());
                subscriber.onNext(words);
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io());
    }
}