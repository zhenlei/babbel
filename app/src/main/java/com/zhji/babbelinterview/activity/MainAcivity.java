package com.zhji.babbelinterview.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.zhji.babbelinterview.R;
import com.zhji.babbelinterview.models.Word;
import com.zhji.babbelinterview.utils.LoadFileUtils;
import com.zhji.babbelinterview.view.GameView;
import com.zhji.babbelinterview.view.GameViewListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;

public class MainAcivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
    }

    @OnClick(R.id.bt_start)
    public void onClickStart(View view) {
        startActivity(new Intent(this, GameActivity.class));
    }
}
