package com.zhji.babbelinterview.activity;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.zhji.babbelinterview.R;
import com.zhji.babbelinterview.models.Word;
import com.zhji.babbelinterview.utils.LoadFileUtils;
import com.zhji.babbelinterview.view.GameView;
import com.zhji.babbelinterview.view.GameViewListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;

public class GameActivity extends AppCompatActivity {

    @Bind(R.id.tv_correct)
    TextView mCorrectView;

    @Bind(R.id.tv_wrong)
    TextView mWrongView;

    @Bind(R.id.tv_time)
    TextView mTimeView;

    @Bind(R.id.tv_score)
    TextView mScoreView;

    @Bind(R.id.tv_timesup)
    TextView mTimesupView;

    @Bind(R.id.tv_word)
    TextView mWordView;

    @Bind(R.id.game_view)
    GameView mGameView;

    int mNumberOfWrong;
    int mNumberOfCorrect;
    int mScore;
    int mTime;

    private void updateScore() {
        mScoreView.setText(String.format("%05d", mScore));
    }

    private void updateCorrect() {
        mCorrectView.setText(String.valueOf(mNumberOfCorrect));
    }

    private void updateWrong() {
        mWrongView.setText(String.valueOf(mNumberOfWrong));
    }

    private void updateTime() {
        if (mTime < 10) {
            mTimeView.setTextColor(Color.RED);
        }
        mTimeView.setText(String.format("%02d", mTime));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        ButterKnife.bind(this);
        getSupportActionBar().hide();

        initValue();

        LoadFileUtils.readFile("words.json")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(words -> startGameView(words),
                        throwable -> Toast.makeText(GameActivity.this, "Failed to load words.", Toast.LENGTH_SHORT).show());
    }

    private void initValue() {
        mTime = 60;
        new CountDownTimer(mTime * 1000, 1000) {

            public void onTick(long millisUntilFinished) {
                mTime--;
                updateTime();
            }

            public void onFinish() {
                mTime = 0;
                updateTime();
                mGameView.stop();
                mGameView.unregisterListener();
                mTimesupView.setVisibility(View.VISIBLE);
            }
        }.start();

        updateScore();
        updateCorrect();
        updateWrong();
        updateTime();
    }


    private void startGameView(List<Word> words) {
        mGameView.registerListener(new GameViewListener() {
            @Override
            public void onMiss(Word word) {
                mNumberOfWrong++;
                updateWrong();
            }

            @Override
            public void onCorrect(Word word, int score) {
                mNumberOfCorrect++;
                updateCorrect();
                mScore += score;
                updateScore();
            }

            @Override
            public void onWrong(Word word) {
                mNumberOfWrong++;
                updateWrong();

            }

            @Override
            public void onNextWord(Word word) {
                mWordView.setText(word.getEnglish());
                mWordView.setFocusable(true);
            }
        });

        mGameView.setConfig(500, 2000, 4000, 10000, words);
        mGameView.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGameView.stop();
        mGameView.unregisterListener();
    }
}
