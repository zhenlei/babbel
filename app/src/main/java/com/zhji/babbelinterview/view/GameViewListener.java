package com.zhji.babbelinterview.view;

import com.zhji.babbelinterview.models.Word;

/**
 * Created by zhenlei on 16/04/16.
 */
public interface GameViewListener {
    public void onMiss(Word word);

    public void onCorrect(Word word, int score);

    public void onWrong(Word word);

    public void onNextWord(Word word);
}