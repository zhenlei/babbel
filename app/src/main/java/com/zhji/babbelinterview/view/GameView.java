package com.zhji.babbelinterview.view;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.plattysoft.leonids.ParticleSystem;
import com.zhji.babbelinterview.R;
import com.zhji.babbelinterview.models.Word;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static android.util.TypedValue.COMPLEX_UNIT_DIP;
import static android.util.TypedValue.applyDimension;

/**
 * Created by zhenlei on 16/04/16.
 */
public class GameView extends FrameLayout {
    // Chance to generate correct word(translation), ex: 10%
    private static final float GEN_CORRECT_WORD_RATE = .2f;
    private static final float SCORE_FACTOR = 500000f;
    List<Word> mOriginalWords;
    List<Word> mFallingWords;
    List<Word> mCorrectWords;
    List<Word> mWrongWords;
    List<Word> mMissingWords;

    HashMap<String, String> mDictionary;
    int mCurrentIndex = 0;

    private int mRateThreshold;
    private int mRate;
    private int mLifespanThreshold;
    private int mLifespan;

    private Handler mHandler;
    private LayoutInflater mInflater;
    private Context mContext;
    private GameViewListener mListener;

    private int mNumberReleasedWorld;
    private long mLastReleasedWord;

    public List<Word> getCorrectWords() {
        return mCorrectWords;
    }

    public List<Word> getWrongWords() {
        return mWrongWords;
    }

    public List<Word> getMissingWords() {
        return mMissingWords;
    }

    public GameView(Context context) {
        super(context);
        init(context);
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public GameView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mCorrectWords = new ArrayList();
        mWrongWords = new ArrayList();
        mMissingWords = new ArrayList();
        mFallingWords = new ArrayList();
        mHandler = new Handler();
    }

    public void registerListener(GameViewListener listener) {
        mListener = listener;
    }

    public void unregisterListener() {
        mListener = null;
    }


    public void setConfig(int rateThreshold, int rate, int lifespanThreshold, int lifespan, List<Word> words) {
        mRateThreshold = rateThreshold;
        mRate = rate;
        mLifespanThreshold = lifespanThreshold;
        mLifespan = lifespan;
        setWords(words);
    }

    private void setWords(List<Word> words) {
        mOriginalWords = words;
        Collections.shuffle(mOriginalWords);
        mDictionary = new HashMap();
        for (Word word : mOriginalWords) {
            mDictionary.put(word.getEnglish(), word.getSpanish());
        }
        mCurrentIndex = -1;
        nextWord();
    }

    private void genFallingWords() {
        mFallingWords = new ArrayList(mOriginalWords);
        Collections.shuffle(mFallingWords);
    }

    private int genRate() {
        return new Random().nextInt(mRateThreshold) + mRate;
    }

    private int genLifespan() {
        return new Random().nextInt(mLifespanThreshold) + mLifespan;
    }

    private Word getFallingWord() {
        if (mFallingWords.isEmpty()) {
            genFallingWords();
        }
        if (new Random().nextFloat() < GEN_CORRECT_WORD_RATE || mNumberReleasedWorld > 1f / GEN_CORRECT_WORD_RATE) {
            mNumberReleasedWorld = 0;
            return mOriginalWords.get(mCurrentIndex);
        } else {
            mNumberReleasedWorld++;
            return mFallingWords.remove(0);
        }
    }

    public void animFallingWord(Word word) {
        View itemView = mInflater.inflate(R.layout.item_word, null);
        final TextView wordButton = (TextView) itemView.findViewById(R.id.bt_word);
        wordButton.setText(word.getSpanish());

        final ViewFlipper feedbackView = (ViewFlipper) itemView.findViewById(R.id.vf_feedback);

        int size = (int) applyDimension(COMPLEX_UNIT_DIP, 120, getResources().getDisplayMetrics());
        int width = new Random().nextInt(getWidth() - size);
        int height = getHeight();

        addView(itemView);
        final AnimatorSet set = new AnimatorSet();
        set.playTogether(ObjectAnimator.ofFloat(itemView, "x", width, width), ObjectAnimator.ofFloat(itemView, "y", 0, height));
        set.setDuration(genLifespan());
        set.start();
        set.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (mListener != null) {
                    removeView(itemView);
                    if (checkMissingWord(word)) {
                        TextView missingView = new TextView(mContext);
                        LayoutParams ll = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                        int marginBottom = (int) applyDimension(COMPLEX_UNIT_DIP, 60, getResources().getDisplayMetrics());
                        ll.setMargins(width, height - marginBottom, 0, 0);

                        missingView.setText(R.string.feedback_missing);
                        missingView.setTextSize(COMPLEX_UNIT_DIP, 12);
                        missingView.setTextColor(Color.RED);
                        missingView.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.anim_feedback));
                        addView(missingView, ll);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                new ParticleSystem((Activity) mContext, 30, R.drawable.confeti, 500)
                                        .setSpeedRange(0.1f, 0.25f)
                                        .oneShot(missingView, 30);
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        removeView(missingView);
                                    }
                                }, 1500);
                            }
                        }, 100);
                    }
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        wordButton.setOnClickListener(v -> {
            feedbackView.setDisplayedChild(checkClickWord(word) ? 1 : 2);

            new ParticleSystem((Activity) mContext, 30, R.drawable.confeti, 500)
                    .setSpeedRange(0.1f, 0.25f)
                    .oneShot(feedbackView, 30);
            v.setEnabled(false);
            v.setVisibility(View.GONE);
            set.pause();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    removeView(itemView);
                }
            }, 1500);
        });

    }

    private boolean checkClickWord(Word word) {
        Word currentWord = mOriginalWords.get(mCurrentIndex);

        if (currentWord.getEnglish().equals(word.getEnglish())) {
            mCorrectWords.add(word);
            float score = SCORE_FACTOR / (System.currentTimeMillis() - mLastReleasedWord);
            mListener.onCorrect(word, Math.round(score));
            nextWord();
            return true;
        } else {
            mListener.onWrong(word);
            mWrongWords.add(word);
            nextWord();
            return false;
        }
    }

    private boolean checkMissingWord(Word word) {
        Word currentWord = mOriginalWords.get(mCurrentIndex);

        if (currentWord.getEnglish().equals(word.getEnglish())) {
            mListener.onMiss(word);
            mMissingWords.add(word);
            nextWord();
            return true;
        } else {
            return false;
        }
    }

    private void nextWord() {
        mLastReleasedWord = System.currentTimeMillis();
        mCurrentIndex++;
        if (mCurrentIndex < mOriginalWords.size()) {
            Word word = mOriginalWords.get(mCurrentIndex);
            mListener.onNextWord(word);

            TextView newWord = new TextView(mContext);
            LayoutParams ll = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            ll.gravity = Gravity.CENTER;
            newWord.setGravity(Gravity.CENTER);
            newWord.setText(word.getEnglish());
            newWord.setTextSize(COMPLEX_UNIT_DIP, 40);
            newWord.setTextColor(mContext.getResources().getColor(R.color.babel_orange));
            newWord.startAnimation(AnimationUtils.loadAnimation(mContext, R.anim.anim_new_word));
            addView(newWord, ll);

        } else {
            stop();
        }
    }

    public void start() {
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animFallingWord(getFallingWord());
                mHandler.postDelayed(this, genRate());
            }
        }, genRate());
    }

    public void stop() {
        mHandler.removeCallbacksAndMessages(null);
    }
}
