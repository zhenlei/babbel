package com.zhji.babbelinterview.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by zhenlei on 16/04/16.
 */
public class Word {
    @SerializedName("text_eng")
    String english;
    @SerializedName("text_spa")
    String spanish;

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    public String getSpanish() {
        return spanish;
    }

    public void setSpanish(String spanish) {
        this.spanish = spanish;
    }

    public Word(String english, String spanish) {
        this.english = english;
        this.spanish = spanish;
    }

    public Word() {

    }

    @Override
    public String toString() {
        // Just for debugging
        return String.format("%s - %s", english, spanish);
    }
}
