package com.zhji.babbelinterview;

import android.app.Application;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;

/**
 * Created by zhenlei on 16/04/16.
 */
public class BabbelApp extends Application {
    private static Context sContext;

    public static Context getContext() {
        return sContext;
    }

    public static void setContext(final Context context) {
        sContext = context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setContext(this);
    }
}
